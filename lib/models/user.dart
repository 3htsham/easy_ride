class MyUser {
  var id;
  var name;
  var email;
  var password;
  var type;
  bool isDriver = false;
  bool isRider = false;
  bool isActive = false;
  String? onGoingRide;

  MyUser({
    this.id,
    this.password,
    this.email,
    this.name,
    this.type,
    this.isDriver = false,
    this.isRider = true,
    this.isActive = false,
    this.onGoingRide,
  });

  MyUser.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    name = data['name'];
    email = data['email'];
    isDriver = data['isDriver'];
    isRider = data['isRider'];
    isActive = data['isActive'];
    type = data['type'];
    onGoingRide = data['ongoing_ride'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = {};
    data['name'] = name;
    data['email'] = email;
    data['id'] = id;
    data['type'] = type;
    data['isDriver'] = isDriver;
    data['isRider'] = isRider;
    data['isActive'] = isActive;
    data['ongoing_ride'] = onGoingRide;
    return data;
  }
}
