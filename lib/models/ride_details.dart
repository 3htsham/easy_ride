import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/models/models.dart';

class RideDetails {
  //Ride ID
  String? id;

  //Marks if the ride is already completed or not
  bool completed = false;

  //Contains Driver Id of the Ride
  String? driverId;

  //Contains DropOff location
  LocationModel? dropOff;

  //Tells if the ride is accepted by some driver or not
  bool? isAccepted;

  //Contains Pickup Location details by users, Will have 2 pickups if shared with other
  LocationModel? pickup;
  LocationModel? pickupTwo;

  //Marks if ride is shareable or not
  bool isShareable = false;

  //Contains the riders of the Ride, Not Driver
  List<String>? users = [];

  //Status of ride
  //0 = finding Driver
  //1 = Driver found
  //2 = Driver Reached
  //3 = Ride Started
  //4 = Ride Completed
  int? rideStatus;

  GeoPoint? driverLocation;


  double? distance = 0;
  double? pickUpDropDistance = 0;

  //Current destination target
  //0 - Pickup1
  //1 - Pickup 2
  //2 - DropOff
  int? target = 0;

  RideDetails({
    this.dropOff,
    this.isShareable = false,
    this.pickup,
    this.users,
    this.id,
    this.completed = false,
    this.pickupTwo,
    this.driverId,
    this.isAccepted,
    this.rideStatus = 0,
    this.driverLocation,
    this.distance = 0,
    this.pickUpDropDistance = 0,
    this.target = 0,
  });

  RideDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    completed = json['completed'];
    driverId = json['driverId'];
    isAccepted = json['isAccepted'];
    isShareable = json['isShareable'];
    rideStatus = json['rideStatus'] ?? 0;
    target = json['target'];
    driverLocation = json['driverLocation'] != null
        ? json['driverLocation'] as GeoPoint
        : null;
    if (json['users'] != null) {
      users = [];
      json['users'].forEach((item) {
        users!.add(item);
      });
    }
    dropOff = LocationModel.fromGeoPoint(json['dropOff']);
    pickup = LocationModel.fromGeoPoint(json['pickup']);
    pickupTwo = json['pickupTwo'] != null
        ? LocationModel.fromGeoPoint(json['pickupTwo'])
        : null;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'completed': completed,
      'driverId': driverId,
      'isAccepted': isAccepted,
      'isShareable': isShareable,
      'rideStatus': rideStatus,
      'driverLocation': driverLocation,
      'target': target,
      'users': users != null ? users!.map((e) => e).toList() : [],
      'dropOff': dropOff!.toGeoPointMap(),
      'pickup': pickup!.toGeoPointMap(),
      'pickupTwo': pickupTwo != null ? pickup!.toGeoPointMap() : null,
    };
  }
}
