import 'package:easy_ride/configs/configs.dart';
import 'package:flutter/material.dart';

class SimpleButton extends StatelessWidget {
  final String title;
  final VoidCallback? onTap;
  const SimpleButton({Key? key, this.title = 'None', this.onTap}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          onTap?.call();
        },
        child: Center(
          child: Text(
            title,
            style: AppTheme.theme!.textTheme.bodyText1,
          ),
        ));
  }
}
