import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../configs/configs.dart';
import '../routes/routes.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {


  @override
  void initState() {
    setUpApp();
    super.initState();
  }

  setUpApp() async {
    Application.prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Application.appName,
      initialRoute: RoutePath.splash,
      onGenerateRoute: RouteGenerator.onGenerateRoute,
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        SizeConfig(context).init();
        var query = MediaQuery.of(context);

        return MediaQuery(
          data: query.copyWith(
            textScaleFactor: 1.0,
            devicePixelRatio: 1.0,
          ),
          child: Theme(
            data:
            // _setting?.brightness == Brightness.light
            //     ?
            ThemeData(
                appBarTheme: AppBarTheme(
                  systemOverlayStyle: SystemUiOverlayStyle.light,
                  color: AppTheme().accentColor(1)
                ),
                brightness: Brightness.light,
                scaffoldBackgroundColor: AppTheme().scaffoldColor(1),
                // primaryColor: config.Colors().scaffoldColor(1),
                primaryColor: Colors.white,
                primaryColorDark: AppTheme().primaryColor(1),
                accentColor: AppTheme().accentColor(1),
                focusColor: AppTheme().focusColor(1),
                hintColor: AppTheme().focusColor(1),
                // cardColor: config.Colors().cardDarkColor(0.03),
                dividerColor: AppTheme().focusColor(0.5),
                canvasColor: AppTheme().focusColor(0.1),
                highlightColor: AppTheme().focusColor(1),
                disabledColor: AppTheme().focusColor(1),
                textTheme: AppTypo.getAppTextTheme(context),
              fontFamily: 'Roboto'
            ),
            child: child!
                // :
            // ThemeData(
            //     appBarTheme: const AppBarTheme(
            //       brightness: Brightness.light,
            //     ),
            //     brightness: _setting?.brightness ?? Brightness.dark,
            //     scaffoldBackgroundColor: AppTheme().scaffoldDarkColor(1),
            //     primaryColor: AppTheme().focusDarkColor(1),
            //     primaryColorDark: AppTheme().primaryColor(1),
            //     accentColor: AppTheme().accentDarkColor(1),
            //     focusColor: AppTheme().focusDarkColor(1),
            //     hintColor: AppTheme().focusDarkColor(1),
            //     cardColor: AppTheme().cardDarkColor(1),
            //     dividerColor: AppTheme().scaffoldColor(0.5),
            //     canvasColor: AppTheme().cardDarkColor(1),
            //     highlightColor: AppTheme().cardDarkColor(1),
            //     disabledColor: AppTheme().disabledDarkColor(1),
            //     textTheme: AppTypo.getAppTextTheme(context, isDark: true)
            // ),
          ),
        );
      },
    );
  }
}
