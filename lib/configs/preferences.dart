class Preferences {
  static const String language = 'LANGUAGE';
  static const String font = 'FONT';
  static const String theme = 'THEME';
  static const String darkOption = 'DARK_OPTION';
  static const String reviewIntro = 'REVIEW';
  static const String currentUser = 'CURRENT_USER';
}