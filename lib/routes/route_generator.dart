import 'package:easy_ride/screens/auth/forget/forget_pass_screen.dart';
import 'package:flutter/material.dart';

import '../screens/screens.dart';
import 'route_path.dart';

class RouteGenerator {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RoutePath.splash:
        return MaterialPageRoute(builder: (context) {
          return const SplashScreen();
        }
            //     builder: (ctx) => ChangeNotifierProvider<HomePageProvider>(
            //   create: (ctx) => HomePageProvider(ctx.read<PrayersService>()),
            //   child: HomePage(),
            // ));
            );
      case RoutePath.login:
        return MaterialPageRoute(builder: (context) {
          return const LoginScreen();
        });
      case RoutePath.signup:
        return MaterialPageRoute(builder: (context) {
          return const SignUpScreen();
        });
      case RoutePath.forgotPass:
        return MaterialPageRoute(builder: (context) {
          return const ForgetPassScreen();
        });
      case RoutePath.home:
        return MaterialPageRoute(builder: (context) {
          return const HomeScreen();
        });
      case RoutePath.rideDetails:
        return MaterialPageRoute(builder: (context) {
          return const RideDetailsScreen();
        });
      case RoutePath.chooseLocation:
        return MaterialPageRoute(builder: (context) {
          return ChooseLocationScreen(
            title: (args?.toString()) ?? ' ',
          );
        });
      case RoutePath.findingDriver:
        return MaterialPageRoute(builder: (context) {
          return const FindingDriverScreen();
        });

      case RoutePath.availableRides:
        return MaterialPageRoute(builder: (context) {
          return const AvailableShareRideScreen();
        });

      case RoutePath.shareRideDetails:
        return MaterialPageRoute(builder: (context) {
          return const ShareRideDetails();
        });

      case RoutePath.trackRide:
        return MaterialPageRoute(builder: (context) {
          return const TrackRideScreen();
        });
      case RoutePath.endRide:
        return MaterialPageRoute(builder: (context) {
          return EndRideScreen(
            paid: args != null ? args as double : 0.0,
          );
        });
      case RoutePath.rateDriver:
        return MaterialPageRoute(builder: (context) {
          return const RateDriverScreen();
        });
      case RoutePath.showRentals:
        return MaterialPageRoute(builder: (context) {
          return const MyRentals();
        });
      default:
        return MaterialPageRoute(
          builder: (context) {
            return Scaffold(
              appBar: AppBar(
                title: const Text('Not Found'),
              ),
              body: Center(
                child: Text('No path for ${settings.name}'),
              ),
            );
          },
        );
    }
  }
}
