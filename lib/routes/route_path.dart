class RoutePath {
  static const String splash = '/Splash';
  static const String login = '/Login';
  static const String signup = '/SignUp';
  static const String forgotPass = '/ForgotPassword';
  static const String home = '/Home';
  static const String rideDetails = '/RideDetails';
  static const String chooseLocation = '/ChooseLocation';

  static const String trackRide = '/TrackRide';
  static const String findingDriver = '/FindingDriver';
  static const String endRide = '/EndRide';
  static const String rateDriver = '/RateDriver';

  static const String availableRides = '/AvailableRides';
  static const String shareRideDetails = '/ShareRideDetails';

  static const String showRentals = '/ShowRentals';
}
