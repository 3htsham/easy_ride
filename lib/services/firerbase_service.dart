import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class FirebaseService extends ChangeNotifier {

  FirebaseAuth? auth;
  FirebaseFirestore? fireStore;
  FirebaseMessaging? _fcm;

  BuildContext context;

  FirebaseService(this.context);

  initialize() async {
    auth = FirebaseAuth.instance;
    fireStore = FirebaseFirestore.instance;
    _fcm = FirebaseMessaging.instance;
    try {
      await _fcm!.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    } catch(e) {}

    try {
      FirebaseMessaging.onMessage.listen((event) {
        UtilLogger.log('FIREBASE SERVICE', event);
        onMessage(event);
      });
      FirebaseMessaging.onMessageOpenedApp.listen((event) {
        UtilLogger.log('FIREBASE SERVICE', event);
        notificationOnLaunch(event);
      });
      UtilLogger.log('FIREBASE SERVICE', "FCM Listeners added");
    } catch (e) {
      UtilLogger.log('FIREBASE SERVICE', '[FirebaseService] initialize(): $e');
    }

    // await FirebaseMessaging.instance.subscribeToTopic('easy_ride');
  }

  Future onMessage(RemoteMessage message) async {
    final data = message.notification;
    UtilLogger.log('FIREBASE SERVICE', message);
    if(data != null) {
      const AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails('high_importance_channel', 'FCM Notifications',
          channelDescription: 'This channel is used for important notifications.',
          importance: Importance.max,
          priority: Priority.high,
          ticker: 'ticker');
      const NotificationDetails platformChannelSpecifics =
      NotificationDetails(
          android: androidPlatformChannelSpecifics,
          iOS: IOSNotificationDetails(presentSound: true),
          macOS: MacOSNotificationDetails(presentSound: true,)
      );
      // context.read<NotificationsService>()
      //     .displayANotification(data.hashCode,
      //     data.title ?? ' ',
      //     data.body ?? ' ',
      //     platformChannelSpecifics);
    }
  }
  Future notificationOnLaunch(RemoteMessage message) async {
    try {
      // if (messageId != message['google.message_id']) {
      //   if (message['data']['id'] == "orders") {
      //     await settingRepo.saveMessageId(message['google.message_id']);
      //     settingRepo.navigatorKey.currentState.pushReplacementNamed('/Pages', arguments: 3);
      //   }
      // }
    } catch (e) {
      // print(CustomTrace(StackTrace.current, message: e));
    }
  }
}