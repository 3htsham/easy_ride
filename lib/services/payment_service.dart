import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/payment_criteria.dart';
import '../utils/utils.dart';
import 'services.dart';

class PaymentService extends ChangeNotifier {
  PayCriteria? criteria;

  BuildContext context;

  PaymentService(this.context);

  getPaymentCriteria() async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference config = fireStore.collection(DatabaseUtil.config);
    final payCriteria = await config.doc(DatabaseUtil.paymentCriteria).get();
    final data = payCriteria.data() as Map<String, dynamic>;
    criteria = PayCriteria.fromJson(data);
    notifyListeners();
  }
}
