import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/enums/enums.dart';
import 'package:easy_ride/models/models.dart';
import 'package:easy_ride/services/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../utils/utils.dart';

class RideService extends ChangeNotifier {
  RideDetails? currentRide;

  bool endedRide = false;

  double toPay = 0.0;

  final uuid = const Uuid();

  BuildContext context;

  RideService(this.context);

  updatePayment(double toPay) {
    this.toPay = toPay;
    notifyListeners();
  }

  createRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides.doc(details.id).set(details.toMap());
    //Add ride to user's profile as well
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    users
        .doc(Application.user!.id)
        .set({'ongoing_ride': details.id}, SetOptions(merge: true));
    Application.user!.onGoingRide == currentRide!.id;
    UtilLogger.log('RideService', "Ride Created Added");
    return details.id;
  }

  listenRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    details.id ??= uuid.v1();
    details.target = RideTarget.pickOne.status;
    final rideDoc = await rides.doc(details.id).get();
    if (!rideDoc.exists) {
      createRide(details);
    }
    rides.doc(details.id).snapshots().listen((snap) async {
      if (snap.exists) {
        final data = snap.data();
        if (data != null) {
          currentRide = RideDetails.fromJson(data as Map<String, dynamic>);
          if(!currentRide!.completed) {
            if(!endedRide) {
              notifyListeners();
              if (currentRide?.rideStatus == RideStatus.completed.status) {
                endedRide = true;
                await completeRide(currentRide!);
              }
            }
          }
        }
      }
    });
  }

  completeRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    details.completed = true;
    await rides.doc(details.id).set(details.toMap());
  }

  getToShareableRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(DatabaseUtil.shareableRides);
    await rides.doc(details.id).set({
      'pickupTwo': details.pickupTwo!.toGeoPointMap(),
      'users': details.users!.map((e) => e).toList(),
      'target': RideTarget.pickTwo.status,
    }, SetOptions(merge: true));
    //Add ride to user's profile as well
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    users
        .doc(Application.user!.id)
        .set({'ongoing_ride': details.id}, SetOptions(merge: true));
    UtilLogger.log('RideService', "Ride Created Added");
    currentRide = details;
    listenRide(details);
    return details.id;
  }

  //Pass rideId from user's profile
  checkOnGoingRide(String rideId) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(DatabaseUtil.rides);
    final rideDoc = await rides.doc(rideId).get();
    if (rideDoc.exists) {
      final data = rideDoc.data() as Map<String, dynamic>;
      currentRide = RideDetails.fromJson(data);
      currentRide!.pickUpDropDistance = CalUtils.calculateDistance(
          currentRide!.dropOff!.latitude!,
          currentRide!.dropOff!.longitude!,
          currentRide!.pickup!.latitude!,
          currentRide!.pickup!.longitude!);
    } else {
      CollectionReference shareableRides =
      fireStore.collection(DatabaseUtil.shareableRides);
      final shareableRide = await shareableRides.doc(rideId).get();
      if (shareableRide.exists) {
        final data = shareableRide.data() as Map<String, dynamic>;
        currentRide = RideDetails.fromJson(data);
        currentRide!.pickUpDropDistance = CalUtils.calculateDistance(
            currentRide!.dropOff!.latitude!,
            currentRide!.dropOff!.longitude!,
            currentRide!.pickup!.latitude!,
            currentRide!.pickup!.longitude!);
      }
    }
    notifyListeners();
    if (currentRide != null) {
      listenRide(currentRide!);
    }
  }

  //TODO: Add Payment Details as well
  endRide(RideDetails details) async {
    final fireStore = context.read<FirebaseService>().fireStore!;
    details.completed = true;
    details.rideStatus = RideStatus.completed.status;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    final rideDoc = await rides.doc(details.id).set(details.toMap());
    currentRide = null;
    //TODO: Add Payment Details as well
    notifyListeners();
  }

  cancelRide(RideDetails details) async {
    //Remove ID from user's profile as well
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference rides = fireStore.collection(
        details.isShareable ? DatabaseUtil.shareableRides : DatabaseUtil.rides);
    await rides.doc(details.id).delete();
    currentRide = null;
    CollectionReference users = fireStore.collection(DatabaseUtil.users);
    users
        .doc(Application.user!.id)
        .set({'ongoing_ride': null}, SetOptions(merge: true));
    Application.user!.onGoingRide = null;
    notifyListeners();
  }
}
