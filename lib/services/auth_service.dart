import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/configs/configs.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/models.dart';
import '../utils/utils.dart';
import 'services.dart';

class AuthService extends ChangeNotifier {
  User? currentUser;

  BuildContext context;

  AuthService(this.context);

  Future<bool> isUserLoggedIn() async {
    currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      Application.user = await getUserDetails();
      final user = Application.user;
      print('Logged In');
    }
    return currentUser != null;
  }

  Future<MyUser> getUserDetails() async {
    final authUser = FirebaseAuth.instance.currentUser;
    CollectionReference users = context
        .read<FirebaseService>()
        .fireStore!
        .collection(DatabaseUtil.users);
    final userDetails = await users.doc(authUser!.uid).get();
    Map<String, dynamic> data = userDetails.data() as Map<String, dynamic>;
    return MyUser.fromJson(data);
  }

  Future<void> addUserToFirestore(
      MyUser myUser, User user, String updatedName) async {
    FirebaseFirestore firStore = FirebaseFirestore.instance;
    CollectionReference users = firStore.collection(DatabaseUtil.users);
    myUser.email = user.email!.toLowerCase();
    myUser.id = user.uid;
    myUser.isActive = false;
    await users.doc(user.uid).set(myUser.toMap());
    UtilLogger.log('AuthService', "User Added");
  }
}
