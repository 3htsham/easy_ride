import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/models/models.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../enums/enums.dart';
import '../utils/utils.dart';
import 'services.dart';

class ShareableRideService extends ChangeNotifier {
  List<RideDetails> rides = [];
  LatLng? myDropOff;

  RideDetails? selectedRide;

  BuildContext context;

  ShareableRideService(this.context);

  selectRide(RideDetails details) {
    selectedRide = details;
    notifyListeners();
  }

  setDropOff(LatLng loc) {
    myDropOff = loc;
    notifyListeners();
  }

  getAvailableRides() async {
    rides.clear();
    notifyListeners();
    final fireStore = context.read<FirebaseService>().fireStore!;
    CollectionReference ridesRef =
        fireStore.collection(DatabaseUtil.shareableRides);
    ridesRef
        .where('rideStatus', isEqualTo: RideStatus.started.status)
        .snapshots()
        .listen((snap) {
      if (snap.docs.isNotEmpty) {
        final docsSnap = snap.docs;
        for (int i = 0; i < docsSnap.length; i++) {
          final doc = docsSnap[i];
          final data = doc.data() as Map<String, dynamic>;
          RideDetails ride = RideDetails.fromJson(data);
          rides.removeWhere((element) => element.id == ride.id);
          final myLoc = myDropOff;
          //Calculating Distance of myy drop off and the ride's drop Off
          ride.distance = CalUtils.calculateDistance(
              myLoc!.latitude,
              myLoc.longitude,
              ride.dropOff!.latitude!,
              ride.dropOff!.longitude!);
          if (ride.distance! < 3) {
            rides.add(ride);
            //Sort by nearby drop off
            rides.sort((a, b) => a.distance! > b.distance! ? 1 : 0);
            notifyListeners();
          }
        }
      }
      UtilLogger.log(
          'SHAREABLE RIDE SERVICE getAvailableRides()', '${rides.length}');
    });
  }
}
