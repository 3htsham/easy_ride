enum RideStatus { searching, driverFound, driverReached, started, completed }

//Status of ride
//0 = finding Driver
//1 = Driver found
//2 = Driver Reached
//3 = Ride Started
//4 = Ride Completed
extension RideStatusTrans on RideStatus {
  int get status {
    switch (this) {
      case RideStatus.searching:
        return 0;
      case RideStatus.driverFound:
        return 1;
      case RideStatus.driverReached:
        return 2;
      case RideStatus.started:
        return 3;
      case RideStatus.completed:
        return 4;
      default:
        return 4;
    }
  }
}

RideStatus getRideStatus(int status) {
  switch (status) {
    case 0:
      return RideStatus.searching;
    case 1:
      return RideStatus.driverFound;
    case 2:
      return RideStatus.driverReached;
    case 3:
      return RideStatus.started;
    case 4:
      return RideStatus.completed;
    default:
      return RideStatus.completed;
  }
}

enum RideTarget {pickOne, pickTwo, dropOff}

extension RideTargetStatus on RideTarget {
  int get status {
    switch(this) {
      case RideTarget.dropOff:
        return 2;
      case RideTarget.pickTwo:
        return 1;
      case RideTarget.pickOne:
        return 0;
      default:
        return 0;
    }
  }
}
RideTarget getRideTargetStatus(int status) {
  switch (status) {
    case 0:
      return RideTarget.pickOne;
    case 1:
      return RideTarget.pickTwo;
    case 2:
      return RideTarget.dropOff;
    default:
      return RideTarget.dropOff;
  }
}
