class DatabaseUtil {
  static const String users = 'users';
  static const String drivers = 'drivers';
  static const String payments = 'payments';
  static const String rides = 'rides';
  static const String shareableRides = 'shareable_rides';
  static const String rentals = 'rentals';
  static const String config = 'config';

  //Configuration Documents' IDs
  static const String paymentCriteria = 'payment_criteria';
}
