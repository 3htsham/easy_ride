import 'package:easy_ride/configs/configs.dart';
import 'package:flutter/material.dart';

class StyleUtils {

  static BoxDecoration getContainerShadowDecoration({Color? background}) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(color: AppTheme().accentColor(0.4),
          offset: const Offset(1,1),
          blurRadius: 10
        ),
      ],
      borderRadius: BorderRadius.circular(15),
      color: background ?? const Color(0xffffffff),
    );
  }

  static BoxDecoration getTopRoundDecoration({Color? background}) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(color: AppTheme().accentColor(0.4),
            offset: const Offset(1,1),
            blurRadius: 10
        ),
      ],
      borderRadius: const BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
      color: background ?? const Color(0xffffffff),
    );
  }

  static EdgeInsets getDefaultPadding({Color? background}) {
    return EdgeInsets.symmetric(horizontal: SizeConfig.responsiveWidth(30),
        vertical: SizeConfig.responsiveHeight(20));
  }

}