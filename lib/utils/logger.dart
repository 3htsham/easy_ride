import 'dart:developer' as developer;
import '../configs/configs.dart';

class UtilLogger {
  static const tag = 'NEPH';

  static log([String tag = tag, dynamic msg]) {
    if (Application.debug) {
      developer.log('$msg', name: tag);
    }
  }

  ///Singleton factory
  static final UtilLogger _instance = UtilLogger._internal();

  factory UtilLogger() {
    return _instance;
  }

  UtilLogger._internal();
}
