import 'dart:math';

import '../models/models.dart';

enum PayType{p1, p2}

class CalUtils {
  static double calculateDistance(double lat1, double lon1, double lat2, double lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

  static double calculatePayment(RideDetails details, double payPerKm, PayType payType) {
    double value = 0.0;
    final dr = details.dropOff!;
    final p1 = details.pickup;
    final p2 = details.pickupTwo;
    if(details.pickup != null && details.pickupTwo == null) {
      //Only one rider there
      //Calculate whole distance
      final distance = calculateDistance(dr.latitude!, dr.longitude!,
          p1!.latitude!, p1.longitude!);
      //Charge for whole distance to P1
      value = distance * payPerKm;
    } if(details.pickup != null && details.pickupTwo != null) {
      //There are two riders
      //Calculate distance of p1-p2
      //Calculate distance of p2-dropOff
      final dsP1P2 = calculateDistance(p2!.latitude!, p2.longitude!,
          p1!.latitude!, p1.longitude!);
      final dsP2dr = calculateDistance(p2.latitude!, p2.longitude!,
          dr.latitude!, dr.longitude!);
      if(payType == PayType.p1) {
        //First rider will pay for
        // - Distance between p1-p2
        // - Half payment of Distance Between p2-dropOff
        final payBwP1P2 = dsP1P2 * payPerKm;
        final payBwP2Dr = (dsP2dr * payPerKm) / 2;
        value = payBwP1P2 + payBwP2Dr;
      } else {
        //Second rider will pay for
        // - Half Payment of Distance Between p2-dropOff
        final payBwP2Dr = (dsP2dr * payPerKm) / 2;
        value = payBwP2Dr;
      }
    }
    return value;
  }
}