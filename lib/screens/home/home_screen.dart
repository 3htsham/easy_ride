import 'package:easy_ride/enums/enums.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as l;
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import '../../configs/configs.dart';
import '../../routes/routes.dart';
import '../../services/services.dart';
import '../../utils/utils.dart';
import '../../widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    if (!await getLocationPermission()) {
      Navigator.of(context).pop();
    }
    Future.delayed(const Duration(seconds: 1), () async {
      await context.read<PaymentService>().getPaymentCriteria();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: SizeConfig.screenWidth,
              height: SizeConfig.responsiveHeight(200),
              decoration:
                  BoxDecoration(color: AppTheme.theme!.accentColor, boxShadow: [
                BoxShadow(
                    color: AppTheme.theme!.accentColor.withOpacity(0.45),
                    offset: const Offset(1, 1),
                    blurRadius: 15)
              ]),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.responsiveWidth(30)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: MediaQuery.of(context).padding.top,
                            ),
                            SizedBox(
                              height: SizeConfig.responsiveHeight(40),
                            ),
                            Text(
                              'EASY RIDE',
                              style: AppTheme.theme!.textTheme.headline1
                                  ?.copyWith(
                                      fontWeight: FontWeight.w600,
                                      wordSpacing: 2,
                                      color: Colors.white),
                            ),
                            Spacer(),
                            Text(
                              'Choose Comfort for extra relaxation'
                                  .toUpperCase(),
                              style:
                                  AppTheme.theme!.textTheme.headline6?.copyWith(
                                      // fontWeight: FontWeight.w600,
                                      wordSpacing: 2,
                                      color: Colors.white),
                            ),
                            SizedBox(
                              height: SizeConfig.responsiveHeight(20),
                            ),
                          ],
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: ClipRRect(
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(10000)),
                              child: Image.asset(
                                UtilAsset.banner,
                                fit: BoxFit.cover,
                              ),
                            ),
                          )
                        ],
                      )),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.responsiveHeight(25),
            ),
            Text(
              "How would you like to continue?",
              style: AppTheme.theme!.textTheme.headline6,
            ),
            if (context.watch<RideService>().currentRide != null)
              Padding(
                padding: EdgeInsets.only(
                    top: SizeConfig.responsiveHeight(10),
                    left: SizeConfig.responsiveHeight(25),
                    right: SizeConfig.responsiveHeight(25)),
                child: MySolidButton(
                  onTap: () {
                    goToRide();
                  },
                  title: 'Track your ongoing ride',
                ),
              ),
            SizedBox(
              height: SizeConfig.responsiveHeight(25),
            ),
            Wrap(
              alignment: WrapAlignment.center,
              direction: Axis.horizontal,
              spacing: 20,
              runAlignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              runSpacing: 20,
              children: [
                InkWell(
                  onTap: () {
                    checkPermissions();
                  },
                  child: Container(
                    decoration: StyleUtils.getContainerShadowDecoration(),
                    height: 150,
                    width: 150,
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Expanded(child: Image.asset(UtilAsset.carIcon)),
                        Text(
                          'Book a Ride',
                          style: AppTheme.theme!.textTheme.bodyText1,
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    checkPermissions(lookingForShareable: true);
                  },
                  child: Container(
                    decoration: StyleUtils.getContainerShadowDecoration(),
                    height: 150,
                    width: 150,
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Expanded(
                            child: Stack(
                          children: [
                            Image.asset(UtilAsset.carIcon),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              child: Icon(
                                CupertinoIcons.person_2_alt,
                                color: AppTheme.theme!.accentColor,
                                size: 36,
                              ),
                            )
                          ],
                        )),
                        Text(
                          'Get Shared Ride',
                          style: AppTheme.theme!.textTheme.bodyText1,
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(RoutePath.showRentals);
                  },
                  child: Container(
                    decoration: StyleUtils.getContainerShadowDecoration(),
                    height: 150,
                    width: 150,
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Expanded(child: Image.asset(UtilAsset.cars)),
                        Text(
                          'Get on Rent',
                          style: AppTheme.theme!.textTheme.bodyText1,
                        )
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    try {
                      await FirebaseAuth.instance.signOut();
                      if (mounted) {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            RoutePath.login, (route) => false);
                      }
                    } catch (e) {
                      UtilLogger.log('HOME SCREEN', 'LOGOUT: $e');
                    }
                  },
                  child: Container(
                    decoration: StyleUtils.getContainerShadowDecoration(),
                    height: 150,
                    width: 150,
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Expanded(
                          child: Icon(
                            CupertinoIcons.power,
                            color: AppTheme.theme!.accentColor,
                            size: 40,
                          ),
                        ),
                        Text(
                          'Logout',
                          style: AppTheme.theme!.textTheme.bodyText1,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  goToRide() async {
    final ride = context.read<RideService>().currentRide!;
    if (ride.rideStatus == RideStatus.searching.status) {
      Navigator.of(context).pushNamed(RoutePath.findingDriver);
    } else {
      Navigator.of(context).pushNamed(RoutePath.trackRide);
    }
  }

  checkPermissions({bool lookingForShareable = false}) async {
    if (await getLocationPermission()) {
      final myLoc = await l.Location().getLocation();
      Application.myLocation = LatLng(myLoc.latitude!, myLoc.longitude!);
      if (!mounted) return;
      if (context.read<RideService>().currentRide == null) {
        if (!lookingForShareable) {
          Navigator.of(context).pushNamed(RoutePath.rideDetails);
        } else {
          Navigator.of(context).pushNamed(RoutePath.availableRides);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("You already have an ongoing ride")));
      }
    }
  }

  Future<bool> getLocationPermission() async {
    l.PermissionStatus permission = await l.Location.instance.hasPermission();
    if (permission == l.PermissionStatus.granted) {
      final enabled = await checkService();
      if (!enabled) {
        if (!mounted) return false;
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("You must turn location services ON")));
      }
      return enabled;
    } else if (permission == l.PermissionStatus.deniedForever) {
      openAppSettings();
      return false;
    } else {
      bool granted = await l.Location().requestService();
      if (granted) {
        final enabled = await checkService();
        if (!enabled) {
          if (!mounted) return false;
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("You must turn location services ON")));
        } else {}
        return enabled;
      } else {
        if (!mounted) return false;
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("You must allow Location Permission")));
        return granted;
      }
    }
  }

  Future<bool> checkService() async {
    bool enabled = await l.Location().serviceEnabled();
    if (!enabled) {
      enabled = await l.Location().requestService();
    }
    return enabled;
  }
}
