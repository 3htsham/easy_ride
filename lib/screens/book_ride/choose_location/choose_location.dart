import 'dart:async';
import 'package:easy_ride/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:geocoding/geocoding.dart';
import 'package:easy_ride/configs/configs.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../models/models.dart';

class ChooseLocationScreen extends StatefulWidget {
  final String title;

  const ChooseLocationScreen({Key? key, required this.title}) : super(key: key);

  @override
  State<ChooseLocationScreen> createState() => _ChooseLocationScreenState();
}

class _ChooseLocationScreenState extends State<ChooseLocationScreen> {
  List<Marker> allMarkers = [];
  GoogleMapController? _mapController;
  LocationModel? selectedLoc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: allMarkers.isEmpty
            ? AppTheme.theme!.primaryColorDark
            : AppTheme.theme!.accentColor,
        title: Text(
          'Select ${widget.title}',
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
        actions: [
          allMarkers.isEmpty
              ? const SizedBox()
              : TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(selectedLoc);
                    },
                  child: Text(
                    'Done',
                    style: AppTheme.theme!.textTheme.subtitle2?.copyWith(color: Colors.white),
                  ))
        ],
      ),
      body: Stack(
        children: [
          SizedBox(
              height: SizeConfig.screenHeight,
              width: SizeConfig.screenWidth,
              child: GoogleMap(
                onMapCreated: (GoogleMapController controller) {
                  _mapController = controller;
                  // setMapStyle(mapStyle);
                },
                initialCameraPosition: const CameraPosition(
                    target: LatLng(33.6780513, 73.1843443), zoom: 12),
                onTap: (pos) {
                  print(pos);
                  setMarker(pos);
                },
                onLongPress: (pos){
                  print(pos);
                  setMarker(pos);
                },
                markers: Set.from(allMarkers),
                zoomControlsEnabled: false,
                // myLocationEnabled: true,
                // myLocationButtonEnabled: true,
              )),
          if (selectedLoc != null)
            Positioned(
              bottom: 0,
              child: Container(
                width: SizeConfig.screenWidth,
                decoration: StyleUtils.getTopRoundDecoration(),
                padding: StyleUtils.getDefaultPadding(),
                child: Text(selectedLoc!.address ?? '   ',
                  style: AppTheme.theme!.textTheme.subtitle2,
                ),
              ),
            )
        ],
      ),
    );
  }

  setMarker(LatLng pos) async {
    addMarker(pos);
    final address = await getAddress(pos);
    setState(() {
      selectedLoc = LocationModel(
          address: address, latitude: pos.latitude, longitude: pos.longitude);
    });
    moveCamera(pos);
  }

  addMarker(LatLng loc) {
    final id = UniqueKey();
    final marker = Marker(
        markerId: MarkerId(id.toString()),
        draggable: false,
        infoWindow: const InfoWindow(title: 'Selected location', snippet: ''),
        position: LatLng(loc.latitude, loc.longitude));
    allMarkers.clear();
    allMarkers.add(marker);
  }

  moveCamera(LatLng loc) {
    _mapController!.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(loc.latitude, loc.longitude),
          zoom: 13,
        ),
      ),
    );
  }

  Future<String> getAddress(LatLng loc) async {
    List<Placemark> placeMarks =
        await placemarkFromCoordinates(loc.latitude, loc.longitude);
    var address =
        "${placeMarks.first.name}, ${placeMarks.first.locality} ${placeMarks.first.country}";
    UtilLogger.log('CHOOSE LOCATION', "Getting user location: 34 $address");
    return address;
  }
}
