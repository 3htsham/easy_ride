import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/enums/enums.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/services/services.dart';
import 'package:location/location.dart' as l;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../../../utils/utils.dart';

class TrackRideScreen extends StatefulWidget {
  const TrackRideScreen({Key? key}) : super(key: key);

  @override
  State<TrackRideScreen> createState() => _TrackRideScreenState();
}

class _TrackRideScreenState extends State<TrackRideScreen> {
  List<Marker> allMarkers = [];
  GoogleMapController? _mapController;

  RideService? serviceProvider;

  String title = '';
  double toPay = 0.0;
  String currency = ' ';

  String driver = 'driver';
  String pickup1 = 'pickup1';
  String pickup2 = 'pickup2';

  bool called = false;

  @override
  void initState() {
    super.initState();
    setListener();
  }

  setListener() {
    serviceProvider = context.read<RideService>();
    serviceProvider!.addListener(listenService);
  }

  @override
  void dispose() {
    serviceProvider?.removeListener(listenService);
    super.dispose();
  }

  listenService() {
    final service = context.read<RideService>();
    final ride = service.currentRide!;
    final drop = ride.dropOff!;
    final dr = ride.driverLocation;
    final p1 = ride.pickup;
    final p2 = ride.pickupTwo;
    final myId = Application.user!.id!;
    //Update Driver Location Everytime, add marker
    //Check if Ride Status Goes to Driver Found, Add your marker
    //Check if Ride Status Goes to Driver Reached, Update title and remove your marker
    //Check if rideStatus Goes to Started, Update Title
    //Check If Target is Pickup Two and Pickup Two available, update Marker of Two
    //Check if Ride Status is Completed, Navigate to end Ride

    addMarker(dr!, driver, 'Driver\'s location',
        markerType:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan), moveCam : true);

    if(ride.rideStatus == RideStatus.driverFound.status) {
      title = 'Your driver is on his way';
      addMarker(GeoPoint(p1!.latitude!, p1.longitude!), pickup1, 'Your location',
          markerType: BitmapDescriptor.defaultMarker);
    } else if(ride.rideStatus == RideStatus.driverReached.status) {
      title = 'Driver has reached to your place';
      //TODO: Vibrate Or Ring
      removeMarker(pickup1);
      setState(() {});
    } else if (ride.rideStatus == RideStatus.started.status) {
      removeMarker(pickup1);
      if(ride.target == RideTarget.pickTwo.status) {
        if(p2!.userId == myId) {
          title = 'Your driver is on the way';
          final distance = CalUtils.calculateDistance(
              p2.latitude!, p2.longitude!, dr.latitude, dr.longitude);
          if (distance < 1) {
            //TODO: Vibrate Or Ring
            title = 'You driver has just reached to your location';
          }
          setState(() {});
        } else {
          title = 'You got another rider to share ride';
        }
        addMarker(GeoPoint(p2.latitude!, p2.longitude!), pickup2,
            p2.userId == myId ? 'Your location' : 'Sharer location',
            markerType: BitmapDescriptor.defaultMarker);
      } else {
        title = 'You are on you way to destination';
        setState(() {});
      }
    }

    if(ride.target == RideTarget.dropOff.status) {
      title = 'You are on you way to destination';
      removeMarker(pickup1);
      removeMarker(pickup2);
      setState(() {});
    }


    /*
    if (ride.rideStatus == RideStatus.driverFound.status &&
        p1 != null &&
        p1.userId == myId) {
      final payCr = context.read<PaymentService>().criteria!;
      final distance = CalUtils.calculateDistance(
          p1.latitude!, p1.longitude!, dr.latitude, dr.longitude);
      toPay = payCr.payPerKm! * distance;
      currency = payCr.currency ?? ' ';
      title = 'Your driver is on his way';
      addMarker(GeoPoint(p1.latitude!, p1.longitude!), pickup1, 'Your location',
          markerType: BitmapDescriptor.defaultMarker);
    }
    if (ride.rideStatus == RideStatus.driverReached.status) {
      title = 'Driver has reached to your place';
      //TODO: Vibrate Or Ring
      removeMarker(pickup1);
      setState(() {});
    }

    if (ride.rideStatus == RideStatus.started.status) {
      removeMarker(pickup1);
      if(ride.target == RideTarget.pickTwo.status) {
        if (p2 != null) {
          if (p2.userId == myId) {
            title = 'Your driver is on the way';
            final distance = CalUtils.calculateDistance(
                p2.latitude!, p2.longitude!, dr.latitude, dr.longitude);
            if (distance < 1) {
              //TODO: Vibrate Or Ring
              title = 'You driver has just reached to your location';
            }
          } else {
            title = 'You got another rider to share ride';
          }
          addMarker(GeoPoint(p2.latitude!, p2.longitude!), pickup2,
              p2.userId == myId ? 'Your location' : 'Sharer location',
              markerType: BitmapDescriptor.defaultMarker);
        }
      } else {
        title = 'You are on you way to destination';
        setState(() {});
      }
    }

    if (ride.target == RideTarget.dropOff.status) {
      removeMarker(pickup1);
      removeMarker(pickup2);
      setState(() {});
    }
    */

    //Set Payment Details for ride
    final payCr = context.read<PaymentService>().criteria!;
    if(p1 != null && p1.userId == myId) {
      toPay = CalUtils.calculatePayment(ride, payCr.payPerKm?.toDouble() ?? 0.0, PayType.p1);
    } else if(p2 != null && p2.userId == myId) {
      toPay = CalUtils.calculatePayment(ride, payCr.payPerKm?.toDouble() ?? 0.0, PayType.p2);
    }
    // if ((p1 != null && p1.userId == myId) && p2 != null) {
    //   final distanceBwPs = CalUtils.calculateDistance(
    //       p2.latitude!, p2.longitude!, p1.latitude!, p1.longitude!);
    //   final distanceBwP2Dr = CalUtils.calculateDistance(
    //       p2.latitude!, p2.longitude!, drop.latitude!, drop.longitude!);
    //
    //   toPay = (payCr.payPerKm! * distanceBwPs) +
    //       ((payCr.payPerKm! * distanceBwP2Dr) / 2);
    //   currency = payCr.currency ?? ' ';
    //   setState(() {});
    // } else if (p2 != null && p2.userId == myId) {
    //   final distance = CalUtils.calculateDistance(
    //       p2.latitude!, p2.longitude!, drop.latitude!, drop.longitude!);
    //   toPay = (payCr.payPerKm! * distance) / 2;
    //   currency = payCr.currency ?? ' ';
    //   setState(() {});
    // } else if (p2 == null && p1 != null) {
    //   final distance = CalUtils.calculateDistance(
    //       p1.latitude!, p1.longitude!, drop.latitude!, drop.longitude!);
    //   toPay = (payCr.payPerKm! * distance);
    //   currency = payCr.currency ?? ' ';
    //   setState(() {});
    // }

    if (ride.rideStatus == RideStatus.completed.status) {
      Application.user!.onGoingRide = null;
      context.read<RideService>().currentRide = null;
      Navigator.of(context).pushReplacementNamed(RoutePath.endRide, arguments: toPay);
      // service.updatePayment(toPay);
    }
  }

  @override
  Widget build(BuildContext context) {
    if(!called) {
      called = true;
      Future.delayed(const Duration(seconds: 2), (){
        listenService();
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('EasyRide Map'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: Stack(
        children: [
          SizedBox(
            height: SizeConfig.screenHeight,
            width: SizeConfig.screenWidth,
            child: GoogleMap(
              onMapCreated: (GoogleMapController controller) {
                _mapController = controller;
                // setMapStyle(mapStyle);
              },
              initialCameraPosition: const CameraPosition(
                  target: LatLng(33.6780513, 73.1843443), zoom: 12),
              onTap: (pos) {},
              onLongPress: (pos) {},
              markers: Set.from(allMarkers),
              zoomControlsEnabled: false,
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: SizeConfig.screenWidth,
              decoration: StyleUtils.getTopRoundDecoration(),
              padding: StyleUtils.getDefaultPadding(),
              child: Column(
                children: [
                  Text(
                    title,
                    style: AppTheme.theme!.textTheme.subtitle2,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  //Map's Configuration
  removeMarker(String id) {
    allMarkers.removeWhere((element) => element.markerId.value == id);
  }

  addMarker(GeoPoint loc, String markerId, String title,
      {BitmapDescriptor? markerType, bool moveCam = false,}) {
    allMarkers.removeWhere((element) => element.markerId.value == markerId);
    final marker = Marker(
        markerId: MarkerId(markerId),
        draggable: false,
        icon: markerType ?? BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(
            title: markerType != null ? 'Drop off Location' : 'Your location',
            snippet: ''),
        position: LatLng(loc.latitude, loc.longitude));
    allMarkers.add(marker);
    setState(() {});
    if(moveCam) {
      moveCamera(loc);
    }
  }

  moveCamera(GeoPoint loc) {
    _mapController?.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(loc.latitude, loc.longitude),
          zoom: 13,
        ),
      ),
    );
  }
}
