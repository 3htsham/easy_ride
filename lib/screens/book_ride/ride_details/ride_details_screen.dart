import 'dart:async';
import 'package:easy_ride/enums/enums.dart';
import 'package:easy_ride/services/services.dart';
import 'package:location/location.dart' as l;
import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/utils/utils.dart';
import 'package:easy_ride/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../models/models.dart';

class RideDetailsScreen extends StatefulWidget {
  const RideDetailsScreen({Key? key}) : super(key: key);

  @override
  State<RideDetailsScreen> createState() => _RideDetailsScreenState();
}

class _RideDetailsScreenState extends State<RideDetailsScreen> {
  //0 index => Pickup Location || 1 index => Drop Off Location
  List<Marker> allMarkers = [];
  final _mapController = Completer<GoogleMapController>();

  LocationModel? pickupLocation;
  LocationModel? dropOffLocation;

  TextEditingController pickupController = TextEditingController();
  TextEditingController dropController = TextEditingController();
  bool isShareable = false;
  String estimatedFare = "0.0";
  String distance = '0.0';
  String currency = '';

  @override
  initState() {
    super.initState();
    checkPermissions();
  }

  createRide() async {
    final user = Application.user;
    LocationModel dropOff = dropOffLocation!;
    dropOff.userId = user!.id;
    LocationModel pickUp = pickupLocation!;
    pickUp.userId = user.id;
    RideDetails details = RideDetails(
      dropOff: dropOff,
      pickup: pickUp,
      isShareable: isShareable,
      users: [user.id!],
      isAccepted: false,
    );
    await context.read<RideService>().listenRide(details);
    //TODO: Navigate to Searching screen and wait for 30 seconds for acceptance
    if (!mounted) return;
    Navigator.of(context).pushReplacementNamed(RoutePath.findingDriver);
  }

  setPickup(LocationModel location) {
    setState(() {
      pickupLocation = location;
    });
    pickupController.text = location.address!;
    addMarker(LatLng(location.latitude!, location.longitude!),
        MarkerType.currentLoc.toString());
    calculateFare();
    setState(() {});
  }

  setDropOff(LocationModel location) {
    setState(() {
      dropOffLocation = location;
    });
    dropController.text = location.address!;
    addMarker(LatLng(location.latitude!, location.longitude!),
        MarkerType.dropOff.toString(),
        markerType:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan));
    calculateFare();
    setState(() {});
  }

  calculateFare() {
    if (pickupLocation != null && dropOffLocation != null) {
      final dist = CalUtils.calculateDistance(
          pickupLocation!.latitude!,
          pickupLocation!.longitude!,
          dropOffLocation!.latitude!,
          dropOffLocation!.longitude!);
      distance = dist.toStringAsFixed(1);
      final payment = context.read<PaymentService>().criteria!;
      currency = payment.currency ?? ' ';
      estimatedFare = (payment.payPerKm! * dist).toStringAsFixed(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Easy Ride Details'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: Stack(
        children: [
          Container(
            color: Colors.white,
            child: GoogleMap(
              onMapCreated: (GoogleMapController _controller) {
                _mapController.complete(_controller);
                // setMapStyle(mapStyle);
              },
              initialCameraPosition: const CameraPosition(
                  target: LatLng(33.6780513, 73.1843443), zoom: 13),
              onTap: (post) {},
              //mapType: MapType.hybrid,
              markers: Set.from(allMarkers),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: SizeConfig.screenWidth,
              decoration: StyleUtils.getTopRoundDecoration(),
              padding: StyleUtils.getDefaultPadding(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Please complete booking details to continue',
                        style: AppTheme.theme!.textTheme.caption
                            ?.copyWith(color: Colors.black45),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(15),
                  ),
                  _buildTextField(
                      controller: pickupController,
                      label: pickupLocation != null
                          ? 'Pickup Location (Tap to change)'
                          : 'Select Pickup Location',
                      icon: CupertinoIcons.location_solid,
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(RoutePath.chooseLocation,
                                arguments: 'Pickup')
                            .then((value) {
                          if (value != null && value is LocationModel) {
                            setPickup(value);
                          }
                        });
                      }),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(10),
                  ),
                  _buildTextField(
                      controller: dropController,
                      label: dropOffLocation != null
                          ? 'Drop Off Location (Tap to change)'
                          : 'Select Drop Off Location',
                      icon: CupertinoIcons.circle,
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(RoutePath.chooseLocation,
                                arguments: 'Drop off')
                            .then((value) {
                          if (value != null && value is LocationModel) {
                            setDropOff(value);
                          }
                        });
                      }),
                  if (pickupLocation != null && dropOffLocation != null)
                    SizedBox(
                      height: SizeConfig.responsiveHeight(10),
                    ),
                  if (pickupLocation != null && dropOffLocation != null)
                    Text('Distance: $distance KMs'),
                  if (pickupLocation != null && dropOffLocation != null)
                    Text('Estimated Fare: $estimatedFare $currency'),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(10),
                  ),
                  Row(
                    children: [
                      Checkbox(
                          value: isShareable,
                          onChanged: (value) {
                            setState(() {
                              isShareable = value ?? false;
                            });
                          }),
                      Text(
                        'Are you willing to share your ride?',
                        style: AppTheme.theme!.textTheme.caption
                            ?.copyWith(color: AppTheme.theme!.accentColor),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(10),
                  ),
                  pickupLocation != null && dropOffLocation != null
                      ? MySolidButton(
                          title: 'Book Easy Ride',
                          onTap: () {
                            createRide();
                          },
                        )
                      : SimpleButton(
                          title: 'Complete Details',
                          onTap: () {},
                        )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTextField(
      {required TextEditingController controller,
      required String label,
      required IconData icon,
      required VoidCallback onTap}) {
    return SizedBox(
      height: 35,
      child: InkWell(
        onTap: () {
          onTap.call();
        },
        child: TextFormField(
          controller: controller,
          enabled: false,
          style:
              AppTheme.theme!.textTheme.caption?.copyWith(color: Colors.black),
          decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              label: Text(label),
              labelStyle: AppTheme.theme!.textTheme.caption
                  ?.copyWith(color: Colors.black),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide:
                    BorderSide(width: 1, color: AppTheme.theme!.accentColor),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide:
                    BorderSide(width: 1, color: AppTheme.theme!.accentColor),
              ),
              prefixIcon: Icon(
                icon,
                size: 18,
                color: AppTheme.theme!.accentColor,
              )),
        ),
      ),
    );
  }

  //Map's Configuration
  addMarker(LatLng loc, String markerId, {BitmapDescriptor? markerType}) {
    allMarkers.removeWhere((element) => element.markerId.value == markerId);
    final marker = Marker(
        markerId: MarkerId(markerId),
        draggable: false,
        icon: markerType ?? BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(
            title: markerType != null ? 'Drop off Location' : 'Your location',
            snippet: ''),
        position: LatLng(loc.latitude, loc.longitude));
    allMarkers.add(marker);
  }

  moveCamera(l.LocationData loc) {
    _mapController.future.then((value) => value.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target:
                  LatLng(loc.latitude ?? 40.416775, loc.longitude ?? -3.70379),
              zoom: 13,
            ),
          ),
        ));
  }

  //Permissions Configuration
  checkPermissions() async {
    if (await getLocationPermission()) {
      final myLoc = await l.Location().getLocation();
      Application.myLocation = LatLng(myLoc.latitude!, myLoc.longitude!);
    } else {
      if (!mounted) return false;
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("You must allow location permission to continue")));
      Navigator.pop(context);
    }
  }

  Future<bool> getLocationPermission() async {
    l.PermissionStatus permission = await l.Location.instance.hasPermission();
    if (permission == l.PermissionStatus.granted) {
      final enabled = await checkService();
      if (!enabled) {
        if (!mounted) return false;
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("You must turn location services ON")));
      }
      return enabled;
    } else if (permission == l.PermissionStatus.deniedForever) {
      openAppSettings();
      return false;
    } else {
      bool granted = await l.Location().requestService();
      if (granted) {
        final enabled = await checkService();
        if (!enabled) {
          if (!mounted) return false;
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("You must turn location services ON")));
        } else {}
        return enabled;
      } else {
        if (!mounted) return false;
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("You must allow Location Permission")));
        return granted;
      }
    }
  }

  Future<bool> checkService() async {
    bool enabled = await l.Location().serviceEnabled();
    if (!enabled) {
      enabled = await l.Location().requestService();
    }
    return enabled;
  }
}
