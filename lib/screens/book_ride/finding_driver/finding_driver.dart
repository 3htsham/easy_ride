import 'package:easy_ride/enums/enums.dart';
import 'package:easy_ride/models/models.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/services/services.dart';
import 'package:easy_ride/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../configs/configs.dart';

class FindingDriverScreen extends StatefulWidget {
  const FindingDriverScreen({Key? key}) : super(key: key);

  @override
  State<FindingDriverScreen> createState() => _FindingDriverScreenState();
}

class _FindingDriverScreenState extends State<FindingDriverScreen> {

  RideService? serviceProvider;

  @override
  void initState() {
    super.initState();
    setListener();
  }

  setListener(){
    serviceProvider = context.read<RideService>();
    serviceProvider!.addListener(listenService);
  }

  @override
  void dispose() {
    serviceProvider?.removeListener(listenService);
    super.dispose();
  }

  listenService(){
    final ride = context.read<RideService>().currentRide!;
      if (ride.rideStatus ==
              RideStatus.driverFound.status) {
        Navigator.of(context).pop();
        Navigator.of(context).pushNamed(RoutePath.trackRide);
      }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RideService>(
      builder: (ctx, service, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Easy Ride Details'),
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: const Icon(CupertinoIcons.back),
            ),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'Finding Your Driver...\nPlease wait...',
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(15),
                ),
                SizedBox(
                  height: 40,
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: AppTheme.theme!.accentColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(25),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.responsiveWidth(50)),
                  child: MySolidButton(
                    onTap: () async {
                      //Cancel Ride Here
                      await context
                          .read<RideService>()
                          .cancelRide(context.read<RideService>().currentRide!);
                      if (mounted) Navigator.of(context).pop();
                    },
                    title: 'Cancel Ride',
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
