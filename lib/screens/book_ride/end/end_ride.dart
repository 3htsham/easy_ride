import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/services/services.dart';
import 'package:easy_ride/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EndRideScreen extends StatefulWidget {
  final double? paid;
  const EndRideScreen({Key? key, this.paid = 0.0}) : super(key: key);

  @override
  State<EndRideScreen> createState() => _EndRideScreenState();
}

class _EndRideScreenState extends State<EndRideScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('EasyRide Map'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushNamedAndRemoveUntil(RoutePath.home, (route) => false);
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.symmetric(vertical: SizeConfig.responsiveHeight(100), horizontal: SizeConfig.responsiveWidth(50)),
              child: Text('Thank you for riding with us', style: AppTheme.theme!.textTheme.headline1,),
            ),
            const Text('You\'ve Paid amount'),
            const SizedBox(height: 10,),
            Text('${context.watch<PaymentService>().criteria?.currency ?? ' '} ${widget.paid?.toStringAsFixed(1)}', style: AppTheme.theme!.textTheme.headline5,),

            const SizedBox(height: 20,),

            Padding(
              padding: EdgeInsets.symmetric(vertical: SizeConfig.responsiveHeight(100), horizontal: SizeConfig.responsiveWidth(50)),
              child: SimpleButton(
                onTap: (){
                  Navigator.of(context).pushNamedAndRemoveUntil(RoutePath.home, (route) => false);
                  },
                title: 'Go Back',
              ),
            )
          ],
        ),
      ),
    );
  }
}
