import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../services/services.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    // await FirebaseAuth.instance.signOut();
    await context.read<FirebaseService>().initialize();
    if (mounted && await context.read<AuthService>().isUserLoggedIn()) {
      //Move to Home Screen
      if(Application.user!.onGoingRide != null) {
        context.read<RideService>().checkOnGoingRide(Application.user!.onGoingRide!);
      }
      //Move to Home Screen
      Navigator.of(context).pushReplacementNamed(RoutePath.home);
    } else {
      Navigator.of(context).pushReplacementNamed(RoutePath.login);
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    AppTheme.theme = theme;

    return Container(
      height: SizeConfig.screenHeight,
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(color: AppTheme.theme!.accentColor),
      child: Center(
        child: SizedBox(
            width: SizeConfig.responsiveWidth(400),
            height: SizeConfig.responsiveHeight(400),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10000),
                child: Image.asset(UtilAsset.logoImg))),
      ),
    );
  }
}
