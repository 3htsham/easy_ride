import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../configs/configs.dart';
import '../../models/models.dart';
import '../../services/services.dart';

class MyRentals extends StatefulWidget {
  const MyRentals({Key? key}) : super(key: key);

  @override
  State<MyRentals> createState() => _MyRentalsState();
}

class _MyRentalsState extends State<MyRentals> {
  @override
  void initState() {
    super.initState();
    context.read<RentalService>().getRentals();
  }

  @override
  Widget build(BuildContext context) {
    final loading = context.watch<RentalService>().isLoading;
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Rentals'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(30),
              vertical: SizeConfig.responsiveHeight(15)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              loading
                  ? SizedBox(
                      height: 100,
                      width: 40,
                      child: Center(
                        child: CircularProgressIndicator(
                          color: AppTheme.theme!.accentColor,
                        ),
                      ),
                    )
                  : context.watch<RentalService>().rentals.isEmpty
                      ? const Padding(
                          padding: EdgeInsets.only(top: 40),
                          child: Text('Nothing to show'),
                        )
                      : Wrap(
                          direction: Axis.horizontal,
                          spacing: SizeConfig.responsiveWidth(30),
                          runSpacing: SizeConfig.responsiveHeight(25),
                          runAlignment: WrapAlignment.start,
                          alignment: WrapAlignment.start,
                          children: List.generate(
                              context.watch<RentalService>().rentals.length,
                              (idx) {
                            final rental =
                                context.watch<RentalService>().rentals[idx];
                            return _buildRentalItem(rental);
                          }),
                        )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRentalItem(RentalModel rental) {
    return InkWell(
      onTap: () {
        _showDetailsDialog(rental);
      },
      child: SizedBox(
        width: (SizeConfig.screenWidth! / 2) -
            (SizeConfig.responsiveWidth(30) * 1.5),
        height: SizeConfig.responsiveHeight(200),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: SizedBox(
                    width: (SizeConfig.screenWidth! / 2) -
                        (SizeConfig.responsiveWidth(25) * 3),
                    child: CachedNetworkImage(
                      imageUrl: rental.imgUrl!,
                      fit: BoxFit.cover,
                    )),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              '${rental.title}',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  _showDetailsDialog(RentalModel rental) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text('Close'))
            ],
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                CachedNetworkImage(
                  imageUrl: rental.imgUrl!,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                Text(
                  rental.title ?? ' ',
                  style: AppTheme.theme!.textTheme.headline5,
                ),
                SizedBox(
                  height: SizeConfig.responsiveHeight(20),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.money_dollar, color: AppTheme.theme!.accentColor, size: 14,),
                    const SizedBox(width: 10,),
                    Text(
                      'Rent Per Day: ${rental.rentPerDay ?? ' '} ${context.watch<PaymentService>().criteria?.currency ?? ' '}',
                      style: AppTheme.theme!.textTheme.bodyText1,
                    ),
                  ],
                ),
                const Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.phone, color: AppTheme.theme!.accentColor, size: 14,),
                    const SizedBox(width: 10,),
                    Text(
                      'Contact: ${rental.contact ?? ' '}',
                      style: AppTheme.theme!.textTheme.bodyText1,
                    ),
                  ],
                ),
                const Divider(),
                Text(
                  'Details:\n\n${rental.description ?? ' '}',
                  style: AppTheme.theme!.textTheme.bodyText2,
                ),
              ],
            ),
          );
        });
  }
}
