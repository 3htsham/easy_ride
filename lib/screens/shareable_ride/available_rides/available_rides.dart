import 'package:easy_ride/configs/configs.dart';
import 'package:easy_ride/models/models.dart';
import 'package:easy_ride/routes/routes.dart';
import 'package:easy_ride/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../services/services.dart';
import 'package:provider/provider.dart';

class AvailableShareRideScreen extends StatefulWidget {
  const AvailableShareRideScreen({Key? key}) : super(key: key);

  @override
  State<AvailableShareRideScreen> createState() =>
      _AvailableShareRideScreenState();
}

class _AvailableShareRideScreenState extends State<AvailableShareRideScreen> {
  ShareableRideService? serviceProvider;
  LocationModel? myDropOff;

  @override
  void initState() {
    super.initState();
    setListener();
  }

  setListener() {
    serviceProvider = context.read<ShareableRideService>();
    serviceProvider!.addListener(listenService);
  }

  @override
  void dispose() {
    serviceProvider?.myDropOff == null;
    serviceProvider?.removeListener(listenService);
    super.dispose();
  }

  void listenService() async {
    // final service = context.read<ShareableRideService>();
    // final rides = service.rides;
    // final dr = service.myDropOff;
    // if(rides.isEmpty && dr != null) {
    //   service.getAvailableRides();
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Available EasyRide to Share'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            myDropOff == null
                ? Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.responsiveHeight(100),
                      horizontal: SizeConfig.responsiveWidth(50),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text(
                          'Please Select your destination to see available Rides to share',
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: SizeConfig.responsiveHeight(15),
                        ),
                        MySolidButton(
                          onTap: () async {
                            Navigator.of(context)
                                .pushNamed(RoutePath.chooseLocation,
                                    arguments: 'Destination')
                                .then((value) {
                              if (value != null) {
                                if (value is LocationModel) {
                                  setState(() {
                                    myDropOff = value;
                                  });
                                  context
                                      .read<ShareableRideService>()
                                      .setDropOff(LatLng(
                                          value.latitude!, value.longitude!));
                                  context
                                      .read<ShareableRideService>()
                                      .getAvailableRides();
                                }
                              }
                            });
                          },
                          title: 'Choose location',
                        ),
                      ],
                    ))
                : Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: SizeConfig.responsiveHeight(20),
                      horizontal: SizeConfig.responsiveWidth(30),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(),
                        Text('Your Destination: ${myDropOff?.address}'),
                        const Divider(),
                        Text(
                          'Nearby Rides to Share',
                          style: AppTheme.theme!.textTheme.headline5,
                        ),
                        SizedBox(
                          height: SizeConfig.responsiveHeight(15),
                        ),
                        context.watch<ShareableRideService>().rides.isEmpty
                            ? const Text('No ride available to share')
                            : ListView.builder(
                                primary: false,
                                shrinkWrap: true,
                                itemCount: context
                                    .watch<ShareableRideService>()
                                    .rides
                                    .length,
                                itemBuilder: (ctx, idx) {
                                  final ride = context
                                      .watch<ShareableRideService>()
                                      .rides[idx];
                                  return ListTile(
                                    onTap: () {
                                      context.read<ShareableRideService>().selectRide(ride);
                                      Navigator.of(context).pushReplacementNamed(RoutePath.shareRideDetails);
                                    },
                                    title: Text(
                                        'Destination: ${ride.dropOff!.address}'),
                                  );
                                },
                              )
                      ],
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
