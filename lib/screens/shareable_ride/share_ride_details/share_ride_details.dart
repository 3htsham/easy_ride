import 'package:easy_ride/models/models.dart';
import 'package:easy_ride/services/services.dart';
import 'package:easy_ride/utils/utils.dart';
import 'package:easy_ride/widgets/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../configs/configs.dart';
import '../../../routes/routes.dart';

class ShareRideDetails extends StatefulWidget {
  const ShareRideDetails({Key? key}) : super(key: key);

  @override
  State<ShareRideDetails> createState() => _ShareRideDetailsState();
}

class _ShareRideDetailsState extends State<ShareRideDetails> {
  LocationModel? myPickup;

  @override
  Widget build(BuildContext context) {
    final service = context.watch<ShareableRideService>();
    final ride = service.selectedRide!;
    final payService = context.watch<PaymentService>();
    String distance = '';
    String cost = '';
    String currency = payService.criteria?.currency ?? ' ';
    final payPerKm = payService.criteria?.payPerKm ?? 0.0;

    if (myPickup != null) {
      //Pick2 & dest distance
      final myDst = CalUtils.calculateDistance(
          myPickup!.latitude!,
          myPickup!.longitude!,
          ride.dropOff!.latitude!,
          ride.dropOff!.longitude!);
      //pick1 & dest distance
      final otherDst = CalUtils.calculateDistance(
          ride.pickup!.latitude!,
          ride.pickup!.longitude!,
          ride.dropOff!.latitude!,
          ride.dropOff!.longitude!);

      //pick1 & pick2 distance
      final dstBet = CalUtils.calculateDistance(ride.pickup!.latitude!,
          ride.pickup!.longitude!, myPickup!.latitude!, myPickup!.longitude!);

      //If (pick1 & dest distance) > (pick2 & pick1 distance) then share between common distance,
      // else pay as per their distances
      if (otherDst > dstBet) {
        final pick1AloneDistance = dstBet;
        final sharedDistance = myDst;
        final pick1Pay = pick1AloneDistance * payPerKm;
        final everyOneWillPay = (sharedDistance * payPerKm) / 2;

        //This user (pickupTwo) will only pay shared
        cost = everyOneWillPay.toStringAsFixed(1);
        distance = sharedDistance.toStringAsFixed(1);
      } else {
        final pick1Pay = otherDst * payPerKm;
        final pick2Pay = myDst * payPerKm;

        cost = pick2Pay.toStringAsFixed(1);
        distance = myDst.toStringAsFixed(1);
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('EasyRide Details'),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(CupertinoIcons.back),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: SizeConfig.responsiveHeight(20),
            horizontal: SizeConfig.responsiveWidth(50),
          ),
          child: Column(
            children: [
              ListTile(
                contentPadding: const EdgeInsets.all(0),
                title: const Text('Ride\'s Destination'),
                subtitle: Text(service.selectedRide?.dropOff?.address ?? ' '),
              ),
              myPickup != null
                  ? ListTile(
                      contentPadding: const EdgeInsets.all(0),
                      title: Text('Distance: $distance'),
                      subtitle: Text('Estimated Fare: $cost $currency'),
                    )
                  : ListTile(
                      dense: true,
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(RoutePath.chooseLocation,
                                arguments: 'Pickup')
                            .then((value) {
                          if (value != null) {
                            if (value is LocationModel) {
                              setState(() {
                                myPickup = value;
                              });
                            }
                          }
                        });
                      },
                      contentPadding: const EdgeInsets.all(0),
                      title: const Text(
                          'Please Choose your pickup location too see Fare Estimation'),
                      trailing: Icon(
                        CupertinoIcons.forward,
                        color: AppTheme.theme!.accentColor,
                      ),
                    ),

              if(myPickup != null)
                SimpleButton(
                  onTap: (){
                    Navigator.of(context)
                        .pushNamed(RoutePath.chooseLocation,
                        arguments: 'Pickup')
                        .then((value) {
                      if (value != null) {
                        if (value is LocationModel) {
                          setState(() {
                            myPickup = value;
                          });
                        }
                      }
                    });
                  },
                  title: 'Change Pickup',
                ),
              if(myPickup != null)
                MySolidButton(
                  onTap: () async {
                    final ride = context.read<ShareableRideService>().selectedRide;
                    ride!.pickupTwo = myPickup;
                    ride.pickupTwo!.userId = Application.user!.id;
                    ride.users!.add(Application.user!.id);
                    await context.read<RideService>().getToShareableRide(ride);
                    if(!mounted) return;
                    Navigator.of(context).pushNamedAndRemoveUntil(RoutePath.trackRide,
                            ModalRoute.withName(RoutePath.home));
                  },
                  title: 'Book Ride',
                ),
            ],
          ),
        ),
      ),
    );
  }
}
