import 'package:easy_ride/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../configs/configs.dart';
import '../../../models/models.dart';
import '../../../services/services.dart';
import '../../../utils/utils.dart';
import '../../../widgets/widgets.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool isLoading = false;
  MyUser user = MyUser();
  GlobalKey<FormState> formKey = GlobalKey();

  _validateForm() async {
    user.isRider = true;
    if (formKey.currentState?.validate() ?? false) {
      formKey.currentState!.save();
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: user.email.toString(),
                password: user.password.toString());

        if (userCredential.user != null) {
          await userCredential.user!.updateDisplayName(user.name);

          await userCredential.user!.sendEmailVerification();

          ///Add user to Firestore
          await addUserToFirestore(userCredential.user!, user.name);

          if (!mounted) return;
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("Check your email for verification")));

          setState(() {
            isLoading = false;
          });

          Navigator.of(context).pushNamed(RoutePath.login);
        }
      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'weak-password') {
          UtilLogger.log('LOGIN', 'The password provided is too weak.');
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("The password provided is too weak.")));
        } else if (e.code == 'email-already-in-use') {
          UtilLogger.log('LOGIN', 'The account already exists for that email.');
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("The account already exists for that email.")));
        }
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        UtilLogger.log('LOGIN', e);
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Error registering new account")));
      }
    }
  }

  Future<void> addUserToFirestore(User user, String updatedName) async {
    return await context
        .read<AuthService>()
        .addUserToFirestore(this.user, user, updatedName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.responsiveWidth(80),
                vertical: SizeConfig.responsiveHeight(150)),
            child: Form(
              key: formKey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("Signup".toUpperCase(),
                        style: AppTheme.theme!.textTheme.headline1
                            ?.copyWith(fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: SizeConfig.responsiveHeight(20),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: SizeConfig.responsiveWidth(100),
                          vertical: SizeConfig.responsiveHeight(30)),
                      decoration: StyleUtils.getContainerShadowDecoration(),
                      child: Column(
                        children: [
                          SizedBox(
                            height: SizeConfig.responsiveHeight(30),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (value) =>
                                (value != null && value.length > 1)
                                    ? null
                                    : "Name can't be empty",
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                                hintText: "Name",
                                hintStyle:
                                    Theme.of(context).textTheme.bodyText1,
                                //border: InputBorder.none
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.black,
                                  ),
                                )),
                            onSaved: (value) {
                              user.name = value;
                            },
                          ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(10),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            validator: (value) => (value != null &&
                                    value.contains("@") &&
                                    value.contains("."))
                                ? null
                                : "Email must be valid",
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle:
                                    Theme.of(context).textTheme.bodyText1,
                                contentPadding: const EdgeInsets.fromLTRB(
                                    20.0, 10.0, 20.0, 10.0),
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.black,
                                  ),
                                )),
                            onSaved: (value) {
                              user.email = value;
                            },
                          ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(10),
                          ),
                          TextFormField(
                            obscureText: true,
                            validator: (value) =>
                                (value != null && value.length >= 6)
                                    ? null
                                    : "Password must contain at least 6 chars",
                            style: Theme.of(context).textTheme.bodyText1,
                            decoration: InputDecoration(
                                hintText: "Password",
                                hintStyle:
                                    Theme.of(context).textTheme.bodyText1,
                                //border: InputBorder.none
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.black,
                                  ),
                                )),
                            onSaved: (value) {
                              user.password = value;
                            },
                          ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(10),
                          ),
                          isLoading
                              ? SizedBox(
                                  height: SizeConfig.responsiveHeight(40),
                                  child: CircularProgressIndicator(
                                      color: AppTheme.theme!.accentColor))
                              : MySolidButton(
                                  title: 'Register',
                                  onTap: () {
                                    _validateForm();
                                  },
                                ),
                          SizedBox(
                            height: SizeConfig.responsiveHeight(10),
                          ),
                          SimpleButton(
                              onTap: () {
                                if (!isLoading) {
                                  Navigator.of(context).pop();
                                }
                              },
                              title: "Already have an account? Login"),
                        ],
                      ),
                    ),
                  ]),
            )),
      ),
    );
  }
}
