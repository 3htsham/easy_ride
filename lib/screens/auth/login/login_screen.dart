import 'dart:convert';

import 'package:easy_ride/services/auth_service.dart';
import 'package:easy_ride/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../configs/configs.dart';
import '../../../models/models.dart';
import '../../../routes/routes.dart';
import '../../../widgets/widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;
  MyUser user = MyUser();
  GlobalKey<FormState> formKey = GlobalKey();

  _validateForm() async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (formKey.currentState?.validate() ?? false) {
      formKey.currentState!.save();

      ///Login user here
      setState(() {
        isLoading = true;
      });

      try {
        UserCredential userCredential = await FirebaseAuth.instance
            .signInWithEmailAndPassword(
                email: user.email.toString(),
                password: user.password.toString());
        setState(() {
          isLoading = false;
        });

        if (userCredential.user != null) {
          final user = await _getUserDetails();
          bool isVerified = userCredential.user!.emailVerified;
          if (isVerified) {
            //Check if user is not admin
            if (user.type == null && user.type != 'admin') {
              // if (user.isActive) {
                //Check if user is Driver or Rider
                if (user.isDriver) {
                  //Not a Rider
                  if (!mounted) return;
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content:
                          Text("You are not allowed to Login to this app")));
                } else {
                  //Navigate to Home Screen
                  Application.user = user;
                  UtilPreferences.setString(Preferences.currentUser, json.encode(user.toMap()));
                  if (!mounted) return;
                  Navigator.of(context).pushReplacementNamed(RoutePath.home);
                }
              // } else {
              //   await FirebaseAuth.instance.signOut();
              //   if (!mounted) return;
              //   ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              //       content:
              //           Text("You account hasn't been approved by admin yet")));
              // }
            } else {
              await FirebaseAuth.instance.signOut();
              if (!mounted) return;
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text("You are not allowed to Login to this app")));
            }

            //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => welcome()));
          } else {
            await userCredential.user!.sendEmailVerification();
            if (!mounted) return;
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text("Check your email for verification")));
            formKey.currentState!.reset();
          }
        }
      } on FirebaseAuthException catch (e) {
        setState(() {
          isLoading = false;
        });
        if (e.code == 'user-not-found') {
          UtilLogger.log('LOGIN', 'No user found for that email.');
          if (!mounted) return;
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text("No user found for that email.")));
        } else if (e.code == 'wrong-password') {
          setState(() {
            isLoading = false;
          });
          UtilLogger.log('LOGIN', 'Wrong password provided for that user.');
          if (!mounted) return;
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("Wrong password provided for that user.")));
        }
      }
    }
  }

  Future<MyUser> _getUserDetails() async {
    return await context.read<AuthService>().getUserDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.responsiveWidth(80),
              vertical: SizeConfig.responsiveHeight(150)),
          child: Form(
            key: formKey,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Login".toUpperCase(),
                      style: AppTheme.theme!.textTheme.headline1
                          ?.copyWith(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: SizeConfig.responsiveHeight(20),
                  ),
                  Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: SizeConfig.responsiveWidth(100),
                          vertical: SizeConfig.responsiveHeight(30)),
                      decoration: StyleUtils.getContainerShadowDecoration(),
                      child: Column(children: [
                        SizedBox(
                          height: SizeConfig.responsiveHeight(30),
                        ),
                        TextFormField(
                          autofocus: false,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) => (value != null &&
                                  value.contains("@") &&
                                  value.contains("."))
                              ? null
                              : "Email must be valid",
                          style: Theme.of(context).textTheme.bodyText1,
                          decoration: InputDecoration(
                            hintText: "Email",
                            hintStyle: Theme.of(context).textTheme.bodyText1,
                            border: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                          ),
                          onSaved: (value) {
                            user.email = value;
                          },
                        ),
                        SizedBox(
                          height: SizeConfig.responsiveHeight(10),
                        ),
                        TextFormField(
                          autofocus: false,
                          obscureText: true,
                          validator: (value) =>
                              (value != null && value.length >= 6)
                                  ? null
                                  : "Password must contain at least 6 chars",
                          style: Theme.of(context).textTheme.bodyText1,
                          decoration: InputDecoration(
                            hintText: "Password",
                            border: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                              ),
                            ),
                            hintStyle: Theme.of(context).textTheme.bodyText1,
                            //border: InputBorder.none
                          ),
                          onSaved: (value) {
                            user.password = value;
                          },
                        ),
                        SizedBox(
                          height: SizeConfig.responsiveHeight(20),
                        ),
                        isLoading
                            ? SizedBox(
                                height: SizeConfig.responsiveHeight(40),
                                child: CircularProgressIndicator(
                                    color: AppTheme.theme!.accentColor))
                            : MySolidButton(
                                title: 'Login',
                                onTap: () {
                                  _validateForm();
                                },
                              ),
                        SizedBox(
                          height: SizeConfig.responsiveHeight(10),
                        ),
                        SimpleButton(
                            onTap: () {
                              if (!isLoading) {
                                Navigator.of(context)
                                    .pushNamed(RoutePath.forgotPass);
                              }
                            },
                            title: "Forgot Password"),
                        SimpleButton(
                            onTap: () {
                              if (!isLoading) {
                                Navigator.of(context)
                                    .pushNamed(RoutePath.signup);
                              }
                            },
                            title: "Don't have account? Register"),
                      ]))
                ]),
          ),
        ),
      ),
    );
  }
}
